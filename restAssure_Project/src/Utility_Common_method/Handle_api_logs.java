package Utility_Common_method;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_api_logs {
	public static void evidence_creator(File dir_name,String File_name,String Endpoint,String requestBody, String responseBody) throws IOException {
		File newFile = new File(dir_name+"\\"+File_name+".txt");
		System.out.println("To Save request and responseBody we Have Created a new File named:"+newFile.getName());
		FileWriter dataWriter = new FileWriter(newFile);
		dataWriter.write("Endpoint is :"+Endpoint+"\n\n");
		dataWriter.write("requestBody is:"+requestBody+"\n\n");
		dataWriter.write("responseBody is :"+responseBody+"\n\n");
		dataWriter.close();	
	}
	public static void evidence_creator(File dir_name,String File_name,String get_Endpoint,String get_responseBody) throws IOException {
		File newFile = new File(dir_name+"\\"+File_name+".txt");
		System.out.println("To Save request and responseBody we Have Created a new File named:"+newFile.getName());
		FileWriter dataWriter = new FileWriter(newFile);
		dataWriter.write("Endpoint is :"+get_Endpoint+"\n\n");
		dataWriter.write("responseBody is :"+get_responseBody+"\n\n");
		dataWriter.close();
		
	}

}
