package Utility_Common_method;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Data_Extractor_Excel {
	public static ArrayList<String> Data_Extractor_reader(String filename, String sheetname, String Tc_name)
			throws IOException {
		ArrayList<String> ArrayData = new ArrayList<String>();
		String project_dir = System.getProperty("user.dir");
		//System.out.println(project_dir);
		// Step 1 create the object of file input stream to locate the data file
		FileInputStream fis = new FileInputStream(project_dir + "\\Data_files\\" + filename + ".xlsx");
		// Step 2 Create the XSSFWorkbook object to open the excel file
		XSSFWorkbook WB = new XSSFWorkbook(fis);
		// Step 3 fetch the number of sheets available in the excel file
		int count = WB.getNumberOfSheets();
		//System.out.println(count);
		//step4 Access the sheet as per inputSheetname
		for (int i = 0; i < count; i++) {
			String Sheetname = WB.getSheetName(i);
			if (Sheetname.equals(sheetname)) {
				System.out.println(Sheetname);
				XSSFSheet sheet = WB.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String TC_name = datarow.getCell(0).getStringCellValue();
					// System.out.println(TC_name);
					if (TC_name.equals(Tc_name)) {
						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String Data = cellvalues.next().getStringCellValue();
							// System.out.println(Data);
							ArrayData.add(Data);
						}
					}
				}
				break;
			}
		}
		WB.close();
		return ArrayData;
	}
}
