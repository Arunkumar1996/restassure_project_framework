package Driver_package;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import Utility_Common_method.Data_Extractor_Excel;

public class Dynamic_driver_class {
	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String>TC_execute = Data_Extractor_Excel.Data_Extractor_reader("Test_data", "Test_cases","TC_name");
		System.out.println(TC_execute);
		int count=TC_execute.size();
		for(int i=1;i<count;i++) {
			String TC_name=TC_execute.get(i);
           System.out.println(TC_name);
        // Call the testcaseclass on runtime by using java.lang.reflect package
         Class<?> Test_class=Class.forName("Test_package."+TC_name);
         // Call the execute method belonging to test class captured in variable TC_name by using java.lang.reflect.method class
         Method execute_method=Test_class.getDeclaredMethod("executor");
         //Set Aceesibility of method is true
         execute_method.setAccessible(true);
         //Create the instance of method capture in variable name
         Object instance_of_class=Test_class.getDeclaredConstructor().newInstance();
        //Excute The Test Script class fetch the Variable Test_class
         execute_method.invoke(instance_of_class);

           
        

		}	
	}

}
