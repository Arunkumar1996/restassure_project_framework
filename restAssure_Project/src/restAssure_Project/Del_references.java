package restAssure_Project;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class Del_references {
	public static void main(String[] args) {
		//Step1: Declare Base Url
		RestAssured.baseURI="https://reqres.in/";

		//Step2:Configure request parameter and trigger API
		String responseBody=given().header("Content-Type","application/json")
		.when().delete("api/users/2").then().log().all().extract().response().asString();
	}

}
