package restAssure_Project;

import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Get_references {
	public static void main(String[] args) {

		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		// Step1: Declare Base Url
		RestAssured.baseURI = "https://reqres.in/";

		// Step2:Configure request parameter and trigger API
		String responsebody = given().header("Content-Type", "application/json").when()
				.get("api/users?page=2").then().extract().response().asString();

		System.out.println("response body is :" + responsebody);
		// Step3: create an object of json path to parse  the response body
		JsonPath jsp_res = new JsonPath(responsebody);
		JSONObject array_res = new JSONObject(responsebody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		int count = dataarray.length();
		System.out.println(count);
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");
			int exp_id = expected_id[i];
			String exp_firstname = expected_firstname[i];
			String exp_lastname = expected_lastname[i];
			String exp_emial = expected_email[i];
			//Step4:Validate Response Body
			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_email, exp_emial);

		}

	}
}
