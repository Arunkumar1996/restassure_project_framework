package restAssure_Project;

import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Patch_references {
	public static void main(String[] args) {
		// Step1: Declare Base Url
		RestAssured.baseURI = "https://reqres.in/";

		// Step2:Configure request parameter and trigger API
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.patch("api/users/2").then().log().all().extract().response().asString();
		// Step3: create an object of json path to parse the requestbody and then the
		// responsebody
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
		// Step4:Validate Response Body
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
	}
}
