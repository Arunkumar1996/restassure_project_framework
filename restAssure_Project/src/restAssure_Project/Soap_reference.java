package restAssure_Project;

import org.testng.Assert;
import io.restassured.path.xml.XmlPath;
import static io.restassured.RestAssured.given;
import io.restassured.RestAssured;

public class Soap_reference {
	public static void main(String[] args) {
		//step1: Declare a Base URL
String BaseURI="https://www.dataaccess.com";
		
		//step2 Declare the request body
		String requestBody="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n"
				+ "    </NumberToWords>\r\n"
				+ "  </soap:Body>\r\n"
				+ "</soap:Envelope>";
		//step3 Trigger the API  and fetch the Response body
		//RestAssured.baseURI=BaseURI;
		String responseBody=given().header("Content-Type", "text/xml; charset=utf-8").body(requestBody).when().post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso")
		.then().extract().response().getBody().asString();
		
		//Step4 print the response body
		System.out.println(responseBody);
		
		//step5 extract the response body parameter
		XmlPath Xml_res=new XmlPath(responseBody);
		String res_tag=Xml_res.getString("NumberToWordsResult");
		System.out.println(res_tag);
		
		//step6 Validate the response body
		Assert.assertEquals(res_tag, "five hundred ");

	}

	}

