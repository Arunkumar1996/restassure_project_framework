package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoint.Put_Endpoint;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.restassured.path.json.JsonPath;
import requestRepository.Put_Request_Repository;

public class put_TC1 extends Common_method_handle_API{
	@Test
	public static void executor() throws IOException {
		 File log_dir = Handle_directory.create_log_directory("put_TC1_logs");
		String put_requestBody=Put_Request_Repository.Put_Request_Tc1();
		String put_endpoint=Put_Endpoint.Put_Enpoint_Tc1();
		for (int i = 0; i < 5; i++) {
			int put_statusCode = Put_statuscode(put_requestBody, put_endpoint);
			System.out.println(put_statusCode);
			if (put_statusCode == 200) {
				String put_responseBody = Put_responseBody(put_requestBody, put_endpoint);
				System.out.println(put_responseBody);
				Handle_api_logs.evidence_creator(log_dir,"put_TC1", put_endpoint, put_requestBody, put_responseBody);
				put_TC1.validator(put_requestBody, put_responseBody);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}
	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
	}
}
