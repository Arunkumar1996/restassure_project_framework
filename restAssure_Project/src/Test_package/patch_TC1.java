package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoint.Patch_Endpoint;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.restassured.path.json.JsonPath;
import requestRepository.Patch_Request_Repository;

public class patch_TC1 extends Common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		 File log_dir = Handle_directory.create_log_directory("patch_TC1_logs");
		String patch_requestBody=Patch_Request_Repository.Patch_request_TC1();
		String patch_endpoint=Patch_Endpoint.Patch_Endpoint_Tc1();
		for (int i = 0; i < 5; i++) {
			int patch_statusCode = Patch_statuscode(patch_requestBody, patch_endpoint);
			System.out.println(patch_statusCode);
			if (patch_statusCode == 200) {
				String patch_responseBody1 = Patch_responseBody(patch_requestBody, patch_endpoint);
				System.out.println(patch_responseBody1);
				Handle_api_logs.evidence_creator(log_dir,"patch_TC1", patch_endpoint, patch_requestBody, patch_responseBody1);
				patch_TC1.validator(patch_requestBody, patch_responseBody1);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}
	public static void validator(String requestBody,String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
	}
		
	}

